<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Welcome to power youth Uganda</title>
    <link rel = "icon" type = "image/png" href = "images/logoic.ico">
    <!-- Latest compiled and minified Bootstrap CSS -->
    <link rel="stylesheet" href="assets/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="main-style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>

    <script src="javascript.js"></script>

    <style>
        .navbar-dark {color: #FFFFFF; border: none; margin: 0; min-height: inherit; border-radius: 0;}
        .navbar-dark .navbar-nav .active>.nav-link, .navbar-dark .navbar-nav .nav-link.active, .navbar-dark .navbar-nav .nav-link.show, .navbar-dark .navbar-nav .show>.nav-link {color: #FFFFFF;}
        .navbar-dark .navbar-nav .nav-link:focus, .navbar-dark .navbar-nav .nav-link:hover {color: sandybrown !important;}
        .navbar-dark .navbar-nav .nav-link {color: #FFFFFF; font-size: 15px; position: relative; font-family: 'Raleway', sans-serif; font-weight: 600; letter-spacing: 1px;}

        .carousel-img-width {width: 100%;}



    </style>

</head>
<body>
<!-- POWER YOUTH UGANDA WEBSITE CONTENT STARTS HERE -->

<div class="container-fluid" style="background-color:#3d6277">
    <div class="container">
        <img class="pyu-logo" src="images/pyulogo.jpg" alt="" >
        <!--start of navigation bar links-->
        <div class="header-nav">
            <nav class="navbar navbar-expand-lg navbar-dark">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">HOME <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="about%20us.php">ABOUT US</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">SERVICES & PROJECTS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">PUBLICATIONS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">CAREERS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="contact%20us.php">CONTACT US</a>
                        </li>
                    </ul>
                    <form class="form-inline my-2 my-lg-0">
                        <input class="form-control mr-sm-2" type="search" placeholder="Search here..." aria-label="Search">
                        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                    </form>
                </div>
            </nav>
        </div>
    </div>
</div>
<!--start of the carousel for the sliding images-->
<div id="demo" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ul class="carousel-indicators">
        <li data-target="#demo" data-slide-to="0" class="active"></li>
        <li data-target="#demo" data-slide-to="1"></li>
        <li data-target="#demo" data-slide-to="2"></li>
    </ul>
    <!-- The slideshow -->
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img src="images/slider1.jpg"  class="carousel-img-width" alt="slider image 1">
            <div class="carousel-caption">
                <h4>Caption 1</h4>
            </div>
        </div>
        <div class="carousel-item">
            <img src="images/slider2.jpg"  class="carousel-img-width" alt="slider image 2">
            <div class="carousel-caption">
                <h4>Caption 2</h4>
            </div>
        </div>
        <div class="carousel-item">
            <img src="images/slider3.jpg"  class="carousel-img-width" alt="slider image 3">
            <div class="carousel-caption">
                <h4>Caption 3</h4>
            </div>
        </div>
    </div>
    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#demo" data-slide="prev">
        <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#demo" data-slide="next">
        <span class="carousel-control-next-icon"></span>
    </a>
</div><br>
 <div class="container">
     <div class="row">
         <div class="col-md-9 offset-md-1">
             <div class="center-grid">
                 <h2>WELCOME TO POWER YOUTH UGANDA</h2><hr>
                 <p>Power Youth Uganda is a Ugandan, non-profit humanitarian organization committed to Youth empowerment, community development
                     and emergency relief projects in central districts of uganda. The aim of Power Youth Uganda is to empower youths and make them
                     active in creating projects</p>
                 <a href="#" class="btn btn-info">Read more</a>
             </div>
         </div>
     </div>
     <br><hr>
     <div class="card-grid">
         <h3 style="text-align: center">WHAT WE DO</h3> <br>
      <div class="row">
         <div class="col-md-4">
             <div class="card mb-3">
                 <img src="images/ca.JPG" alt="">
                 <div class="card-body">
                     <h6 class="card-title">CAREER GUIDANCE</h6>
                     provides career guidance to the students of secondary schools
                     <a  href="#">Read More</a>
                 </div>
             </div>
         </div>
         <div class="col-md-4">
             <div class="card mb-3">
                 <img src="images/ca.JPG" alt="">
                 <div class="card-body">
                     <h6 class="card-title">INNOVATIONS WORKSHOP</h6>
                     prepare and organize workshops where youths can participate
                     <a  href="#">Read More</a>
                 </div>
             </div>
         </div>
         <div class="col-md-4">
             <div class="card mb-3">
                 <img src="images/ca.JPG" alt="">
                 <div class="card-body">
                     <h6 class="card-title">AIDS/HIV COUNSELLING</h6>
                     provides counselling seminars to youths
                     <a  href="#">Read More</a>
                 </div>
             </div>
         </div>
      </div>
     </div>
 </div>
<br>
<div class="container" style="background-color: #bfffff">
    <div class="row">
        <div class="col-md-10 offset-md-1">
            <div class="grid-titles">
                <h4>OUR PARTNERS</h4><hr>
            </div>
        </div>
    </div>
</div><br>

<!--start of the footer-->
 <footer class="mainfooter" role="contentinfo">
    <div class="footer-top ">
        <div class="container">
            <div class="row">
                <div class="col-md-7 offset-md-3"><br><br>
                    <div style="text-align: justify">
                        <div class="join">
                            <h4>Join the TEAM</h4>
                        </div>
                        <p>Be part of the POWER YOUTH UGANDA Team that struggles to empower youths socially, politically and economically in different productive fields
                            where youths can engage and change their lives. </p>
                        <div class="join">
                            <p>Register with us to join the TEAM</p>
                            <a href="#" class="btn btn-info"><strong>REGISTER AND JOIN</strong></a>
                        </div>
                    </div>
                </div>
            </div>
        </div><br>
    </div>
    <div class="footer-middle">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="footer-pad">
                        <h6>USEFUL LINKS</h6>
                        <ul class="list-unstyled">
                            <li><a href="#"></a></li>
                            <li><a href="#">Ministry of Labour and Gender</a></li>
                            <li><a href="#">TASO Uganda</a></li>
                            <li><a href="#">Uganda Human Rights Commission</a></li>
                            <li><a href="#">Uganda National Students Association</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="footer-pad">
                        <h6>SERVICES & PROJECTS</h6>
                        <ul class="list-unstyled">
                            <li><a href="#">Power Youth innovation Workshop</a></li>
                            <li><a href="#">Chalk making project</a></li>
                            <li><a href="#">Macos bricking laying project</a></li>
                            <li><a href="#">Power Youth community OutReach</a></li>
                            <li><a href="#">Power Youth blood donation</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="footer-pad">
                        <h6>ADDRESS</h6>
                        <address>
                            <ul class="list-unstyled">
                                <li>
                                    Main Office is located at
                                </li>
                                <li>
                                    Kamagwa House<br>
                                    Elgin Street<br>
                                    Masaka Town<br>
                                </li>
                                <li>
                                    +256 751483218 | +256 774364199
                                </li>
                                <li>
                                    <i class="fa fa-envelope-o"></i>  poweryouthuganda@gmail.com
                                </li>
                            </ul>
                        </address>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12"><br>
                    <p class="center-grid">&copy; Copyright 2018 - Power Youth Uganda.  All rights reserved.</p>
                </div>
            </div>
        </div>
    </div>
 </footer>

<!-- POWER YOUTH UGANDA WEBSITE CONTENT ENDS HERE -->

<!-- jQuery library -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

<!-- Popper -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

<!-- Latest compiled and minified Bootstrap JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</body>
</html>