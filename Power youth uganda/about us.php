<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>About the Power Youth Uganda</title>
    <link rel = "icon" type = "image/png" href = "images/logoic.ico">
    <!-- Latest compiled and minified Bootstrap CSS -->
    <link rel="stylesheet" href="assets/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="main-style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>


    <style>
        .navbar-dark {color: #FFFFFF; border: none; margin: 0; min-height: inherit; border-radius: 0;}
        .navbar-dark .navbar-nav .active>.nav-link, .navbar-dark .navbar-nav .nav-link.active, .navbar-dark .navbar-nav .nav-link.show, .navbar-dark .navbar-nav .show>.nav-link {color: #FFFFFF;}
        .navbar-dark .navbar-nav .nav-link:focus, .navbar-dark .navbar-nav .nav-link:hover {color: sandybrown !important;}
        .navbar-dark .navbar-nav .nav-link {color: #FFFFFF; font-size: 15px; position: relative; font-family: 'Raleway', sans-serif; font-weight: 600; letter-spacing: 1px;}

        .about-message{
            text-align: justify;
        }

        .director > h5{
            font-size: 13px;
            padding-top: 1em;
        }
        .director > h6{
            font-size: 11px;
        }



    </style>

</head>
<body>
<!-- POWER YOUTH UGANDA WEBSITE CONTENT STARTS HERE -->

<div class="container-fluid" style="background-color:#3d6277">
    <div class="container">
        <img class="pyu-logo" src="images/pyulogo.jpg" alt="" >
        <!--start of navigation bar links-->
        <div class="header-nav">
            <nav class="navbar navbar-expand-lg navbar-dark">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="home.php">HOME <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="about%20us.php">ABOUT US</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">SERVICES & PROJECTS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">PUBLICATIONS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">CAREERS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="contact%20us.php">CONTACT US</a>
                        </li>
                    </ul>
                    <form class="form-inline my-2 my-lg-0">
                        <input class="form-control mr-sm-2" type="search" placeholder="Search here..." aria-label="Search">
                        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                    </form>
                </div>
            </nav>
        </div>
    </div>
</div>


<br>
    <div class="row">
        <div class=" col-md-3 offset-md-1" style="text-align: center">
            <div class="director">
                <img src="images/direct.jpg" alt="director">
                <h5>Mr. KATUMBA VICENT</h5>
                <h6>CEO & DIRECTOR,</h6>
                <h6>POWER YOUTH UGANDA</h6>
            </div>
        </div>
        <div class="col-md-7">
            <div class="center-grid">
                <div class="about-message">
                    <p>POWER YOUTH UGANDA is a Ugandan, non-profit humanitarian organization committed to Youth empowerment, community development
                        and emergency relief projects in central districts of uganda. It was primarily established to empower youths and make them
                        active in creating projects. Power Youth Uganda opened its main Head Offices in february 2014 (located in Masaka town) which is currently the handles fundraising,
                        donor management, volunteer programs and marketing campaigns for all major projects. In August 2015, Power Youth Uganda established
                        its first branch in Kampala city <i>(capital city of Uganda)</i> to network and partner with other organizations as well as obtain grants from Uganda government ministries
                        and agencies.</p>
                    <p>Power Youth Uganda is best know for its productive projects and foundation works towards social and economic transformation of ouths lives
                        and aims to build to a new generation of youth through engaging them in productive matters as an asset that can shape humanness, character and civility.
                        The Organization is mission-driven with national,regional and a global reach and has active chapters in United Kingdom (UK).
                    </p>
                    <p style="text-align: center">
                        <a class="btn btn-info btn-sm" data-toggle="collapse" href="#collapseMission" role="button" aria-expanded="false" aria-controls="collapseExample">VIEW MISSION</a>
                        <a class="btn btn-info btn-sm" data-toggle="collapse" href="#collapseVision" role="button" aria-expanded="false" aria-controls="collapseExample">VIEW VISION</a>
                        <a class="btn btn-info btn-sm" data-toggle="collapse" href="#collapseCoreValues" role="button" aria-expanded="false" aria-controls="collapseExample">VIEW CORE VALUES</a>
                    </p>
                    <div class="row">
                        <div class="col-md-11 offset-md-1">
                            <div class="organ-structure">
                                <div class="collapse" id="collapseMission">
                                    <div class="card card-body">
                                        <h6> OUR MISSION</h6>
                                        To  power and enhance the dignity and quality of life of youths by eliminating barriers to opportunity and helping them
                                        in need reach their fullest potential through the power of work.
                                    </div>
                                </div>
                                <div class="collapse" id="collapseVision">
                                    <div class="card card-body">
                                        <h6>OUR VISION</h6>
                                        Every youth has the opportunity to achieve his/her fullest potential and participate in and contribute to all aspects of life
                                    </div>
                                </div>
                                <div class="collapse" id="collapseCoreValues">
                                    <div class="card card-body">
                                        <h6>OUR CORE VALUES</h6>
                                        <ul>
                                            <li><b>Creativity:</b>  Instill our work with imagination and innovation.</li>
                                            <li><b>Respect:</b>     Treat all Youths with dignity and respect.</li>
                                            <li><b>Learning:</b>    Challenge each other to strive for excellence and to continually learn. </li>
                                            <li><b>Discovery:</b>   Explore and bring to light new knowledge and ideas, and better ways of doing work.</li>
                                            <li><b>Integrity:</b>   Carry out all work with the greatest responsibility and accountability.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div
<br><br>
<div class="container">
    <div class="row">
        <div class="col-md-10 offset-md-1">
            <div class="organ-structure">
                <h5>ORGANIZATION STRUCTURE OF THE POWER YOUTH UGANDA</h5>
                <p>Power Youth Uganda is headed by the Board of Member which is a main body of the Organization for the major and decisions, directions
                    and implementation within the organization. The Organization currently has 4 departments and each is responsible for its activities</p>
            </div>
        </div>
    </div>

</div>


<br><br>
<!--start of the footer-->
 <footer class="mainfooter">
    <div class="footer-top ">
        <div class="container">
            <div class="row">
                <div class="col-md-7 offset-md-3"><br><br>
                    <div style="text-align: justify">
                        <div class="join">
                            <h4>Join the TEAM</h4>
                        </div>
                        <p>Be part of the POWER YOUTH UGANDA Team that struggles to empower youths socially, politically and economically in different productive fields
                            where youths can engage and change their lives. </p>
                        <div class="join">
                            <p>Register with us to join the TEAM</p>
                            <a href="#" class="btn btn-info"><strong>REGISTER AND JOIN</strong></a>
                        </div>
                    </div>
                </div>
            </div>
        </div><br>
    </div>
    <div class="footer-middle">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="footer-pad">
                        <h6>USEFUL LINKS</h6>
                        <ul class="list-unstyled">
                            <li><a href="#"></a></li>
                            <li><a href="#">Ministry of Labour and Gender</a></li>
                            <li><a href="#">TASO Uganda</a></li>
                            <li><a href="#">Uganda Human Rights Commission</a></li>
                            <li><a href="#">Uganda National Students Association</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="footer-pad">
                        <h6>SERVICES & PROJECTS</h6>
                        <ul class="list-unstyled">
                            <li><a href="#">Power Youth innovation Workshop</a></li>
                            <li><a href="#">Chalk making project</a></li>
                            <li><a href="#">Macos bricking laying project</a></li>
                            <li><a href="#">Power Youth community OutReach</a></li>
                            <li><a href="#">Power Youth blood donation</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="footer-pad">
                        <h6>ADDRESS</h6>
                        <address>
                            <ul class="list-unstyled">
                                <li>
                                    Main Office is located at
                                </li>
                                <li>
                                    Kamagwa House<br>
                                    Elgin Street<br>
                                    Masaka Town<br>
                                </li>
                                <li>
                                    +256 751483218 | +256 774364199
                                </li>
                                <li>
                                    poweryouthuganda@gmail.com
                                </li>
                            </ul>
                        </address>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12"><br>
                    <p class="center-grid">&copy; Copyright 2018 - Power Youth Uganda.  All rights reserved.</p>
                </div>
            </div>
        </div>
    </div>
 </footer>

<!-- POWER YOUTH UGANDA WEBSITE CONTENT ENDS HERE -->

<!-- jQuery library -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

<!-- Popper -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

<!-- Latest compiled and minified Bootstrap JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</body>
</html>